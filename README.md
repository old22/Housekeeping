# Housekeeping

Rust WASM + NPM package that runs a Web page
which listens to an Arduino HTTP server that
sends sensor data and displays it on the Web.

# The Arduino server and its data

The Arduino server consists of an Arduino One,
and Ethernet shield and an LM35DZ temperature
sensor. The server has a fixed private IP address
192.168.1.177 and listens to any requests, sending
back a CORS response with a JSON object with the
fields clock (usize - server time [ms] -) and data
(float - sensor temperature [ºC] -). This Rust
WASM package makes a request to this fixed IP
address every 10 seconds, updating the Web content
on success.

The IP/URL is stored at the `src/lib.rs` ``URL``
variable, and the Rust struct mimicking the
server's JSON object at `src/query.rs`
``ArduinoData`` structure. Changing these elements
may adapt the application to user requirements.

# Requirements

* Rust (cargo)
* wasm-pack
* NPM (optional)

# Building and running (Linux (Bash) + NPM)

The `build.sh` and `run.sh` Bash scripts are
provided in order to shorten the process:

```bash
$ bash build.sh
$ bash run.sh
```

# Building and running (Any)

First, build the package using `wasm-pack`:

```bash
wasm-pack build --target web
```

The output files are stored at `pkg` directory.
Copy allfiles into the `hello` directory and change
the `index.html` packe import line ...
```javascript
import init, { update_data } from "./node_modules/housekeeping/housekeeping.js";
```
... to:
```javascript
import init, { update_data } from "./housekeeping.js";
```

Finally, run a local server on the hello directory
using your favourite program.


# Bibliography

* [Wasm by example](https://wasmbyexample.dev/examples/hello-world/hello-world.rust.en-us.html)
* [Rust `wasm-bindgen` book](https://rustwasm.github.io/docs/wasm-bindgen/)
* [Rust `wasm-bindgen` reference](https://docs.rs/wasm-bindgen)