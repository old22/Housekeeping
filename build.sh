wasm-pack build --target web;
pushd pkg || (echo "No pkg dir." && exit 1);
npm link;
cd ../hello  || (echo "No hello dir?" && exit 1);
npm link housekeeping;
npm install;
popd || (echo "Empty dir stack?" && exit 1);