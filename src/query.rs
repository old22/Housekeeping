use wasm_bindgen::{JsValue, JsCast};
use wasm_bindgen_futures::JsFuture;
use web_sys::{Request, RequestInit, RequestMode, Response};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct ArduinoData {
    pub clock: usize,
    pub data: f32,
}

#[derive(Debug, Serialize, Deserialize)]
struct JsString {
    msg: String
}

/// Request sensor measures.
///
/// Make an HTTP CORS request and get
/// ArduinoData struct. In case of
/// error, return Js error.
///
/// Parameters:
/// -----------
///
/// * `url` - Arduino URL address.
pub async fn get_data(url: &str) -> Result<ArduinoData, JsValue> {
    // Set-up request options.
    let mut opts: RequestInit = RequestInit::new();
    opts.method("GET");
    opts.mode(RequestMode::Cors);

    // Set-up request.
    let request : Request = Request::new_with_str_and_init(url, &opts)?;

    // Set-up additional headers.
    request.headers().set("Accept", "application/json")?;

    // Make request.
    let window = web_sys::window().unwrap();
    let resp_value = JsFuture::from(window.fetch_with_request(&request)).await?;

    // Unwrap response safely.
    if ! resp_value.is_instance_of::<Response>() {
        return Err(JsValue::from("Returned value is not response type."));
    }
    let resp: Response = resp_value.dyn_into().unwrap();

    // Get JSON content.
    let json: JsValue = JsFuture::from(resp.json()?).await?;

    // Check that JSON data complies with ArduinoData struct.
    let data: ArduinoData = json.into_serde()
        .map_err(|e| JsValue::from(e.to_string()))?;

    Ok(data)

    /*
    // Re-convert JSON data.
    Ok(JsValue::from_serde(&data)
        .unwrap_or(JsValue::from("Error struct->Js."))
    )
    */
}
