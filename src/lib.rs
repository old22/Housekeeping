mod utils;
mod query;

use wasm_bindgen::prelude::*;
use web_sys::{Document, Element, Window, HtmlElement};
use crate::utils::set_panic_hook;
use query::{ArduinoData, get_data};

const URL: &str = "http://192.168.1.177";

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
extern {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub fn greet() {
    alert("Hello, housekeeping!");
}

#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
    let window: Window = web_sys::window()
        .ok_or(JsValue::from("No global window."))?;
    let document: Document = window.document()
        .ok_or(JsValue::from("should have a document on window"))?;
    let body: HtmlElement = document.body()
        .expect("document should have a body");
    let p_result: Option<Element> = document
        .get_element_by_id("data");
    if p_result.is_none() {
        let val: Element = document.create_element("p")?;
        val.set_inner_html("Hello from Rust!");
        val.set_attribute("id", "data")?;

        body.append_child(&val)?;
    }
    Ok(())
}

/// Update HTML sensor data display.
///
/// Make an HTTP request to Arduino and update
/// 'data' HTML paragraph element with new data
/// content. In case of error, return Js error.
#[wasm_bindgen]
pub async fn update_data() -> Result<(), JsValue> {
    // In case of panic, get full error content.
    set_panic_hook();

    // Get paragraph element.
    let window: Window = web_sys::window()
        .ok_or(JsValue::from("No global window."))?;
    let doc: Document = window.document()
        .ok_or(JsValue::from("should have a document on window"))?;
    let p: Element = doc.get_element_by_id("data")
        .ok_or(JsValue::from("No 'data' ID found."))?;

    // Get requested data.
    let data: ArduinoData = get_data(URL).await?;

    // Set paragraph.
    let msg: String = format!("Clock: {}, Data: {:4.3}", data.clock, data.data);
    p.set_inner_html(msg.as_str());
    Ok(())
}